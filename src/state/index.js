import { combineReducers } from "redux";
import { reducer as merchants } from "./merchants";
import { reducer as transactions } from "./transactions";

export default combineReducers({
  merchants,
  transactions
});
