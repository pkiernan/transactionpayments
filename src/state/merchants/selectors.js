import { createSelector } from "reselect";
import { find } from "lodash";

const getState = ({ merchants = {} }) => merchants;

export const selectorMerchants = createSelector(
  getState,
  ({ merchants = [] }) => merchants || []
);

export const selectorIsLoaded = createSelector(
  getState,
  ({ isLoaded }) => isLoaded === true
);

export const selectorSelectedMerchantId = createSelector(
  getState,
  ({ selectedMerchant }) => selectedMerchant
);

export const selectorSelectedMerchantData = createSelector(
  selectorSelectedMerchantId,
  selectorMerchants,
  (id, merchants) => find(merchants, { id })
);
