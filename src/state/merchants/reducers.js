import { LOAD_MERCHANTS } from "./types";
import { get } from "lodash";

const initState = {
  isLoaded: false,
  merchants: [],
  selectedMerchant: null
};

const formatMerchantData = ({ merchant_id: id, name, _links }) => ({
  id,
  name,
  href: get(_links, "self.href", "").split(".json")[0]
});

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_MERCHANTS.CALL: {
      return initState;
    }
    case LOAD_MERCHANTS.SUCCESS: {
      const data = get(payload, "data");
      const merchants = data.map(formatMerchantData);
      return {
        ...state,
        isLoaded: true,
        merchants
      };
    }
    case LOAD_MERCHANTS.FAIL: {
      return {
        ...initState,
        isLoaded: true
      };
    }
    default: {
      return state;
    }
  }
};
