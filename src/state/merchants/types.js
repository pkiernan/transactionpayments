import { createActionType } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const LOAD_MERCHANTS = createActionType({
  id: "LOAD_MERCHANTS",
  type: ACTION_API
});
