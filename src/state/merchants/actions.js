import { LOAD_MERCHANTS } from "./types";
import { createAction } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const actionLoadMerchants = createAction({
  id: LOAD_MERCHANTS,
  type: ACTION_API
});
