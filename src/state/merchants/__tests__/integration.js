import { LOAD_MERCHANTS } from "../types";
import { actionLoadMerchants } from "../actions";
import * as selectors from "../selectors";
import { runStateIntegrationTests } from "../../../utils/test";

const successPayload = {
  data: [
    {
      merchant_id: "one",
      name: "One",
      _links: {
        self: {
          href: "/merchants/one.json"
        }
      }
    },
    {
      merchant_id: "two",
      name: "Two",
      _links: {
        self: {
          href: "/merchants/two.json"
        }
      }
    }
  ]
};
const successMerchants = [
  {
    id: "one",
    name: "One",
    href: "/merchants/one"
  },
  {
    id: "two",
    name: "Two",
    href: "/merchants/two"
  }
];

const config = {
  id: "merchants",
  selectors,
  defaultValues: {
    isLoaded: null,
    merchants: null,
    selectedMerchant: null
  },
  actions: [
    {
      id: LOAD_MERCHANTS.CALL,
      action: actionLoadMerchants.call,
      expectedValues: {
        selectorIsLoaded: false,
        selectorMerchants: [],
        selectorSelectedMerchantId: null
      }
    },
    {
      id: LOAD_MERCHANTS.SUCCESS,
      action: actionLoadMerchants.success,
      payload: successPayload,
      expectedValues: {
        selectorIsLoaded: true,
        selectorMerchants: successMerchants,
        selectorSelectedMerchantId: null
      }
    },
    {
      id: LOAD_MERCHANTS.FAIL,
      action: actionLoadMerchants.fail,
      expectedValues: {
        selectorIsLoaded: true,
        selectorMerchants: [],
        selectorSelectedMerchantId: null
      }
    }
  ]
};

runStateIntegrationTests(config);
