import { takeLatest, put } from "redux-saga/effects";
import { actionLoadMerchants } from "./actions";
import { LOAD_MERCHANTS } from "./types";
import { callAPI } from "../../utils/services";

/* 
  DEVCOMMENT 
  I had trouble loading the json files due to CORs issues.
  In the interest of saving time I have copied a subset of the data to a local server via json-server
*/
function* getMerchantSummary() {
  try {
    const response = yield callAPI({ url: "/merchant_summary" });
    yield put(actionLoadMerchants.success(response));
  } catch (e) {
    yield put(actionLoadMerchants.fail(e));
  }
}

export function* watchMerchantsLoad() {
  yield takeLatest(LOAD_MERCHANTS.CALL, getMerchantSummary);
}
