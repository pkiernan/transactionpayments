import { LOAD_TRANSACTIONS } from "./types";
import { calculateSubsidy } from "../../utils/calculations";

const initState = {
  name: "",
  id: null,
  discount_cutoff: 0,
  subsidy: 0,
  discount_subsidy: 0,
  transactions: [],
  isLoaded: false
};

const transformSuccessData = ({
  name,
  merchant_id: id,
  transactions,
  pricing = {}
}) => {
  const { discount_cutoff, subsidy, discount_subsidy } = pricing;
  return {
    name,
    id,
    discount_cutoff,
    discount_subsidy,
    subsidy,
    transactions: transactions.map(({ date, description, price }) => ({
      date,
      description,
      price,
      subsidy: calculateSubsidy({
        price,
        discount_cutoff,
        subsidy,
        discount_subsidy
      })
    }))
  };
};

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_TRANSACTIONS.CALL: {
      return {
        ...initState
      };
    }
    case LOAD_TRANSACTIONS.SUCCESS: {
      return {
        ...state,
        isLoaded: true,
        ...transformSuccessData(payload.data)
      };
    }
    case LOAD_TRANSACTIONS.FAIL: {
      return {
        ...initState,
        isLoaded: true
      };
    }
    default:
      return state;
  }
};
