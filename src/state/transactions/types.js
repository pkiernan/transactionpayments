import { createActionType } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const LOAD_TRANSACTIONS = createActionType({
  id: "LOAD_TRANSACTIONS",
  type: ACTION_API
});
