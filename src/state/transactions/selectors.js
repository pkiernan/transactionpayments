import { createSelector } from "reselect";

const getState = ({ transactions = {} }) => transactions;

export const selectorName = createSelector(getState, ({ name = "" }) => name);
export const selectorId = createSelector(getState, ({ id = null }) => id);
export const selectorDiscountCutoff = createSelector(
  getState,
  ({ discount_cutoff = 0 }) => discount_cutoff
);
export const selectorSubsidy = createSelector(
  getState,
  ({ subsidy = 0 }) => subsidy
);
export const selectorDiscountSubsidy = createSelector(
  getState,
  ({ discount_subsidy = 0 }) => discount_subsidy
);
export const selectorIsLoaded = createSelector(
  getState,
  ({ isLoaded }) => isLoaded === true
);
export const selectorTransactions = createSelector(
  getState,
  ({ transactions = [] }) => transactions
);
export const selectorTotalSubsidy = createSelector(
  selectorTransactions,
  transactions =>
    Number(
      transactions.reduce((total, { subsidy }) => total + subsidy, 0).toFixed(2)
    )
);
// DEVCOMMENT - not sure if the total is meant to be the total of prices only, or include the subsidy? assuming the former
export const selectorGrossPrice = createSelector(
  selectorTransactions,
  transactions =>
    Number(
      transactions.reduce((total, { price }) => total + price, 0).toFixed(2)
    )
);
// DEVCOMMENT - if the latter then I would use this instead
export const selectorTotalPrice = createSelector(
  [selectorTotalSubsidy, selectorGrossPrice],
  (subsidy, price) => Number(price + subsidy).toFixed(2)
);
