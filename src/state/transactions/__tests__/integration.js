import { LOAD_TRANSACTIONS } from "../types";
import { actionLoadTransactions } from "../actions";
import * as selectors from "../selectors";
import { runStateIntegrationTests } from "../../../utils/test";

const name = "Merchant";
const merchant_id = "merch_id";
const subsidy = 10;
const discount_subsidy = 5;
const discount_cutoff = 100;
const transactions = [
  {
    description: "Tr One",
    date: "2019-03-10T11:50:20.250Z",
    price: 100
  },
  {
    description: "Tr Two",
    date: "2019-03-10T11:50:20.250Z",
    price: 200
  }
];

const successPayload = {
  data: {
    name,
    merchant_id,
    pricing: {
      subsidy,
      discount_subsidy,
      discount_cutoff
    },
    transactions
  }
};

const config = {
  id: "transactions",
  selectors,
  defaultValues: {
    name: "",
    id: null,
    discount_cutoff: 0,
    subsidy: 0,
    discount_subsidy: 0,
    transactions: [],
    isLoaded: false
  },
  actions: [
    {
      id: LOAD_TRANSACTIONS.CALL,
      action: actionLoadTransactions.call,
      payload: successPayload,
      expectedValues: {
        selectorName: "",
        selectorId: null,
        selectorDiscountCutoff: 0,
        selectorSubsidy: 0,
        selectorDiscountSubsidy: 0,
        selectorTransactions: [],
        selectorIsLoaded: false
      }
    },
    {
      id: LOAD_TRANSACTIONS.SUCCESS,
      action: actionLoadTransactions.success,
      payload: successPayload,
      expectedValues: {
        selectorName: name,
        selectorId: merchant_id,
        selectorDiscountCutoff: discount_cutoff,
        selectorSubsidy: subsidy,
        selectorDiscountSubsidy: discount_subsidy,
        selectorTransactions: transactions,
        selectorIsLoaded: true
      }
    },
    {
      id: LOAD_TRANSACTIONS.FAIL,
      action: actionLoadTransactions.fail,
      expectedValues: {
        selectorName: "",
        selectorId: null,
        selectorDiscountCutoff: 0,
        selectorSubsidy: 0,
        selectorDiscountSubsidy: 0,
        selectorTransactions: [],
        selectorIsLoaded: true
      }
    }
  ]
};

runStateIntegrationTests(config);
