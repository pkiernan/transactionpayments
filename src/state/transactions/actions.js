import { LOAD_TRANSACTIONS } from "./types";
import { createAction } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const actionLoadTransactions = createAction({
  id: LOAD_TRANSACTIONS,
  type: ACTION_API
});
