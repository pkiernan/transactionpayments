import { takeLatest, put } from "redux-saga/effects";
import { actionLoadTransactions } from "./actions";
import { LOAD_TRANSACTIONS } from "./types";
import { callAPI } from "../../utils/services";

/* 
  DEVCOMMENT 
  I had trouble loading the json files due to CORs issues.
  In the interest of saving time I have copied a subset of the data to a local server via json-server
*/
function* getTransactionData({ payload = {} }) {
  const { href: url } = payload;
  try {
    const response = yield callAPI({ url });
    yield put(actionLoadTransactions.success(response));
  } catch (e) {
    yield put(actionLoadTransactions.fail(e));
  }
  yield false;
}

export function* watchTransactionsLoad() {
  yield takeLatest(LOAD_TRANSACTIONS.CALL, getTransactionData);
}
