export const calculateSubsidy = ({
  price,
  discount_cutoff,
  subsidy,
  discount_subsidy
}) => {
  /* DEVCOMMENT
  From instructions:
  `The merchant would pay a 7% subsidy for transactions below £750. Transactions of £750 or more pay only 4% subsidy.`
  Presuming this means for a cutoff of £100, only £99.99p would be at full subsidy, and £100+ at discount
  */
  const cutoff = discount_cutoff - 0.01;
  const discountSubsidyAmount =
    price > cutoff ? ((price - cutoff) * discount_subsidy) / 100 : 0;
  const fullSubsidyAmount = ((price > cutoff ? cutoff : price) * subsidy) / 100;
  const subsidyTotal = discountSubsidyAmount + fullSubsidyAmount;
  // DEVCOMMENT - not sure what rounding factor is appropriate, choosing to round down in the customer's favour
  return Math.floor(subsidyTotal * 100) / 100;
};
