import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { createStoreWithState } from "../store";
import Enzyme, { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
Enzyme.configure({ adapter: new Adapter() });

export const runStateIntegrationTests = ({
  id: reducerId,
  selectors,
  defaultValues = {},
  actions = []
}) =>
  describe(`${reducerId} state integration`, () => {
    actions.map(
      ({ id: actionId, action, payload = null, expectedValues = {} }) => {
        describe(`${actionId} updates the store correctly`, () => {
          let store;
          let state;
          beforeEach(() => {
            store = createStoreWithState({ isMocked: true });
            store.dispatch(action(payload));
            state = store.getState();
          });
          Object.keys(expectedValues).map(key =>
            test(`${key} returns correct value`, () => {
              const selector = selectors[key];
              const actualValue = selector(state);
              const expectedValue = expectedValues[key];
              if (Array.isArray(expectedValue)) {
                expect(actualValue).toMatchObject(expectedValue);
              } else {
                expect(actualValue).toBe(expectedValue);
              }
            })
          );
        });
      }
    );
  });

export const runComponentIntegrationTests = ({
  id: componentId,
  Component,
  actions = []
}) =>
  describe(`${componentId} integration`, () => {
    actions.map(
      ({ id: actionId, action, expectedProps = {}, payload = null }) => {
        describe(`${actionId} updates the props correctly`, () => {
          let store;
          let wrapper;
          let component;
          beforeEach(() => {
            store = createStoreWithState();
            wrapper = mount(
              <Provider store={store}>
                <Router>
                  <Component />
                </Router>
              </Provider>
            );
            store.dispatch(action(payload));
            wrapper.update();
            component = wrapper.find(Component).childAt(0);
          });
          test("renders the component", () => {
            expect(wrapper.find(Component).length).toEqual(1);
          });
          Object.keys(expectedProps).map(key => {
            test(`prop:${key} has correct value`, () => {
              const actualValue = component.prop(key);
              const expectedValue = expectedProps[key];
              if (Array.isArray(expectedValue)) {
                expect(actualValue).toMatchObject(expectedValue);
              } else {
                expect(actualValue).toBe(expectedValue);
              }
            });
          });
        });
      }
    );
  });
