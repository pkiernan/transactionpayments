import { ACTION_API, ACTION_UI } from "../constants";

const CALL_OPERAND = "call";
const SUCCESS_OPERAND = "success";
const FAIL_OPERAND = "fail";

const getActionNameSpace = ({ type = "", id = "", operand = "" }) =>
  `${type}/${id}${operand.length ? `/${operand}` : ""}`;

export const createActionType = ({ type = ACTION_UI, id = "" }) => {
  switch (type) {
    case ACTION_API:
      return {
        CALL: getActionNameSpace({ type, id, operand: CALL_OPERAND }),
        SUCCESS: getActionNameSpace({ type, id, operand: SUCCESS_OPERAND }),
        FAIL: getActionNameSpace({ type, id, operand: FAIL_OPERAND })
      };
    case ACTION_UI:
    default:
      return getActionNameSpace({ type, id });
  }
};

export const createAction = ({ type = ACTION_UI, id = "" }) => {
  switch (type) {
    case ACTION_API:
      return {
        call: payload => ({
          type: id.CALL,
          payload
        }),
        success: payload => ({
          type: id.SUCCESS,
          payload
        }),
        fail: payload => ({
          type: id.FAIL,
          payload
        })
      };
    case ACTION_UI:
    default:
      return payload => ({
        type: id,
        payload
      });
  }
};
