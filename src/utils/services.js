import axios from "axios";

const root_url = "http://localhost:3001";

export function* callAPI({ url = "", method = "get", data = {} }) {
  const config = {
    method,
    data,
    url: `${root_url}${url}`,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json"
    }
  };
  return yield axios(config);
}
