import { all, fork } from "redux-saga/effects";
import { watchMerchantsLoad } from "../state/merchants/services";
import { watchTransactionsLoad } from "../state/transactions/services";

export default function* root() {
  yield all([fork(watchMerchantsLoad), fork(watchTransactionsLoad)]);
}
