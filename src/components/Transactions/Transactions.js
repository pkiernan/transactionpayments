import React, { Component, Fragment } from "react";
import PieChart from "react-minimal-pie-chart";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  selectorId,
  selectorName,
  selectorSubsidy,
  selectorDiscountCutoff,
  selectorDiscountSubsidy,
  selectorTransactions,
  selectorIsLoaded,
  selectorGrossPrice,
  selectorTotalSubsidy
} from "../../state/transactions/selectors";
import TransactionsTable from "./TransactionsTable";
import "./styles.scss";

class Transactions extends Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    id: PropTypes.string,
    name: PropTypes.string,
    subsidy: PropTypes.number,
    discountCutoff: PropTypes.number,
    discountSubsidy: PropTypes.number,
    transactions: PropTypes.array
  };
  renderPieChart() {
    const { totalPrice, totalSubsidy } = this.props;
    // DEVCOMMENT - ran out of time to create a canvas from scratch
    return (
      <PieChart
        data={[
          { title: "Subsidy", value: 10, color: "#666" },
          { title: "Price", value: 15, color: "#efefef" }
        ]}
      />
    );
  }
  renderHeading() {
    const { name, transactions, totalPrice, totalSubsidy } = this.props;
    return (
      <header className="transactions-header">
        <div className="header-data">
          <h1>Transactions</h1>
          <h2>{name}</h2>
          <ul className="transaction-data">
            <li>
              <span className="transaction-data__label">Count</span>
              <span className="transaction-data__value">
                {transactions.length}
              </span>
            </li>
            <li>
              <span className="transaction-data__label">Total</span>
              <span className="transaction-data__value">{`£${totalPrice}`}</span>
            </li>
            <li>
              <span className="transaction-data__label">Subsidy</span>
              <span className="transaction-data__value">{`£${totalSubsidy}`}</span>
            </li>
          </ul>
        </div>
        <div className="header-graph">{this.renderPieChart()}</div>
      </header>
    );
  }
  renderTransactions() {
    const {
      transactions,
      subsidy,
      discountCutoff,
      discountSubsidy
    } = this.props;
    const props = {
      transactions,
      subsidy,
      discountSubsidy,
      discountCutoff
    };
    return <TransactionsTable {...props} />;
  }
  renderLoading() {
    const { isLoaded } = this.props;
    // DEVCOMMENT - or loading wheel animation or icon etc.
    return !isLoaded ? <span className="loading-wheel">loading</span> : null;
  }
  renderContent() {
    const { isLoaded } = this.props;
    return isLoaded ? (
      <Fragment>
        {this.renderHeading()}
        {this.renderTransactions()}
      </Fragment>
    ) : null;
  }
  render() {
    return (
      <section className="transactions">
        {this.renderLoading()}
        {this.renderContent()}
      </section>
    );
  }
}
const mapDispatchToProps = dispatch => ({});

const mapStateToProps = state => ({
  isLoaded: selectorIsLoaded(state),
  id: selectorId(state),
  name: selectorName(state),
  subsidy: selectorSubsidy(state),
  discountCutoff: selectorDiscountCutoff(state),
  discountSubsidy: selectorDiscountSubsidy(state),
  transactions: selectorTransactions(state),
  totalPrice: selectorGrossPrice(state),
  totalSubsidy: selectorTotalSubsidy(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);
