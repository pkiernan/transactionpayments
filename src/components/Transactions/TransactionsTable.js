import React, { Component } from "react";
import moment from "moment";
import PropTypes from "prop-types";
import "./styles.scss";

class TransactionsTable extends Component {
  static propTypes = {
    subsidy: PropTypes.number,
    discountCutoff: PropTypes.number,
    discountSubsidy: PropTypes.number,
    transactions: PropTypes.array
  };
  renderHeading() {
    return (
      <thead>
        <tr>
          <th>Description</th>
          <th>Date</th>
          <th>Price (£)</th>
          <th>Subsidy (£)</th>
        </tr>
      </thead>
    );
  }
  renderBody() {
    const { transactions } = this.props;
    const rows = transactions.map((transaction, index) => {
      const { price, description, date, subsidy } = transaction;
      return (
        <tr key={index}>
          <td>{description}</td>
          <td>{moment(date).format("YYYY-MM-DD, H:mm")}</td>
          <td>{price}</td>
          <td>{subsidy}</td>
        </tr>
      );
    });
    return <tbody>{rows}</tbody>;
  }
  render() {
    return (
      <main className="transactions-table">
        <table>
          {this.renderHeading()}
          {this.renderBody()}
        </table>
      </main>
    );
  }
}
export default TransactionsTable;
