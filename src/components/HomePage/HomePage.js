import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Merchants } from "../Merchants";
import { Transactions } from "../Transactions";
import { actionLoadTransactions } from "../../state/transactions/actions";
import { selectorMerchants } from "../../state/merchants/selectors";
import { selectorId } from "../../state/transactions/selectors";
import "./styles.scss";

class HomePage extends Component {
  static propTypes = {
    loadTransactions: PropTypes.func.isRequired
  };
  componentDidUpdate(oldProps) {
    const { merchants } = this.props;
    if (merchants.length && merchants !== oldProps.merchants) {
      this.loadMerchantTransactions(merchants[0]);
    }
  }
  loadMerchantTransactions(merchantData = {}) {
    const { loadTransactions } = this.props;
    loadTransactions(merchantData);
  }
  renderMerchants() {
    const { selectedMerchantId } = this.props;
    return (
      <Merchants
        selectedId={selectedMerchantId}
        onMerchantClick={this.loadMerchantTransactions.bind(this)}
      />
    );
  }
  renderTransactions() {
    return <Transactions />;
  }
  render() {
    return (
      <main className="home-page">
        {this.renderMerchants()}
        {this.renderTransactions()}
      </main>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadTransactions: merchantData =>
    dispatch(actionLoadTransactions.call(merchantData))
});

const mapStateToProps = state => ({
  merchants: selectorMerchants(state),
  selectedMerchantId: selectorId(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
