import React, { Component } from "react";
import PropTypes from "prop-types";

class MerchantItem extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    isSelected: PropTypes.bool.isRequired
  };
  render() {
    const { id, name, href, onClick, isSelected } = this.props;
    return (
      <li className="merchant-item">
        <button disabled={isSelected} onClick={() => onClick({ id, href })}>
          {name}
        </button>
      </li>
    );
  }
}
export default MerchantItem;
