import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { actionLoadMerchants } from "../../state/merchants/actions";
import {
  selectorIsLoaded,
  selectorMerchants
} from "../../state/merchants/selectors";
import MerchantItem from "./MerchantItem";
import "./styles.scss";

class Merchants extends Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    merchants: PropTypes.array.isRequired,
    loadMerchants: PropTypes.func.isRequired,
    onMerchantClick: PropTypes.func.isRequired,
    selectedId: PropTypes.string
  };
  componentDidMount() {
    const { loadMerchants } = this.props;
    loadMerchants();
  }
  renderLoading() {
    const { isLoaded } = this.props;
    // DEVCOMMENT - or loading wheel animation or icon etc.
    return !isLoaded ? <span className="loading-wheel">loading</span> : null;
  }
  renderMerchantList() {
    const { isLoaded, merchants, onMerchantClick, selectedId } = this.props;
    console.log("* selectedId:", selectedId);
    console.log("* merchants:", merchants);
    const merchantList = merchants.map(data => (
      <MerchantItem
        key={data.id}
        {...data}
        isSelected={selectedId === data.id}
        onClick={() => onMerchantClick(data)}
      />
    ));
    return isLoaded ? <ul className="merchants-list">{merchantList}</ul> : null;
  }
  render() {
    return (
      <section className="merchants">
        <h1>Merchants</h1>
        {this.renderLoading()}
        {this.renderMerchantList()}
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadMerchants: () => dispatch(actionLoadMerchants.call())
});

const mapStateToProps = state => ({
  isLoaded: selectorIsLoaded(state),
  merchants: selectorMerchants(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(Merchants);
